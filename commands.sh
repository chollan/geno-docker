
docker run -d \
  --name=sabnzbd \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=America\New_York \
  -p 8080:8080 \
  -p 9090:9090 \
  -v /opt/Docker/nzbget/data:/config \
  -v /opt/Docker/shared/downloads:/downloads \
  -v /opt/Docker/nzbget/incomplete-downloads:/incomplete-downloads \
  --restart unless-stopped \
  ghcr.io/linuxserver/sabnzbd


docker run -d \
  --name=radarr \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=America/New_York \
  -p 7878:7878 \
  -v /opt/Docker/radarr/config:/config \
  -v /data:/watsong \
  -v /opt/Docker/shared/downloads:/downloads \
  --restart unless-stopped \
  ghcr.io/linuxserver/radarr

docker run -d \
  --name=sonarr \
  -e PUID=0 \
  -e PGID=0 \
  -e TZ=America/New_York \
  -p 8989:8989 \
  -v /opt/Docker/sonarr/config:/config \
  -v /data:/tv \
  -v /opt/Docker/shared/downloads:/downloads \
  --restart unless-stopped \
  linuxserver/sonarr:latest

docker run -d \
  --name=lidarr \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=America/New_York \
  -p 8686:8686 \
  -v /opt/Docker/lidarr/config:/config \
  -v /data:/music \
  -v /opt/Docker/shared/downloads:/downloads \
  --restart unless-stopped \
  ghcr.io/linuxserver/lidarr

docker run -d \
  --name readarr \
  -p 8787:8787 \
  -e PUID=1000 \
  -e PGID=1000 \
  -e UMASK=002 \
  -e TZ=America/New_York \
  -e ARGS="" \
  -e DEBUG="no" \
  -v /opt/Docker/readarr/config:/config \
  -v /opt/Docker/shared/downloads:/downloads \
  -v /data:/data \
  --restart unless-stopped \
  hotio/readarr:nightly


docker run -d \
    --cap-add=NET_ADMIN \
    -p 6789:6789 \
    --name=nzbgetvpn \
    -v /opt/Docker/nzbgetvpn/data:/data \
    -v /opt/Docker/nzbgetvpn/config:/config \
    -v /etc/localtime:/etc/localtime:ro \
    -v /opt/Docker/shared/downloads:/downloads  \
    -e VPN_ENABLED=yes \
    -e VPN_USER=p5178071 \
    -e VPN_PASS=ni4ZacQSGX \
    -e VPN_PROV=pia \
    -e STRICT_PORT_FORWARD=yes \
    -e ENABLE_PRIVOXY=yes \
    -e LAN_NETWORK=192.168.0.0/24 \
    -e NAME_SERVERS=209.222.18.222,37.235.1.174,1.1.1.1,8.8.8.8,209.222.18.218,37.235.1.177,1.0.0.1,8.8.4.4 \
    -e DEBUG=false \
    -e UMASK=000 \
    -e PUID=0 \
    -e PGID=0 \ 
    jshridha/docker-nzbgetvpn:latest


docker run -d \
  --name=ombi \
  -e PUID=1000 \
  -e PGID=1000 \
  -e TZ=America/New_York \
  -p 3579:3579 \
  -v /opt/Docker/ombi/config:/config \
  --restart unless-stopped \
  ghcr.io/linuxserver/ombi

docker run -d \
  --name=overseerr \
  -e LOG_LEVEL=info \
  -e TZ=America/New_York \
  -e PROXY=no \
  -p 5055:5055 \
  -v /opt/Docker/overseerr/config:/app/config \
  --restart unless-stopped \
  sctx/overseerr 

docker run -d \
  --name=plex \
  -p 32400:32400 \
  -p 1900:1900/udp \
  -p 3005:3005 \
  -p 8324:8324 \
  -p 32410:32410/udp \
  -p 32412:32412/udp \
  -p 32413:32413/udp \
  -p 32414:32414/udp \
  -p 32469:32469 \
  -e PUID=1000 \
  -e PGID=1000 \
  -e VERSION=docker \
  -v /opt/Docker/plex/config:/config \
  -v /data:/data \
  --restart unless-stopped \
  ghcr.io/linuxserver/plex

docker volume create owncloud_data
docker volume create owncloud_config
docker run -d \
  --name nextcloud \
  -p 8080:80 \
  -v owncloud_data:/var/www/html/data \
  -v owncloud_config:/var/www/html/config \
  --add-host=watsong:192.168.0.102 \
  nextcloud:latest

docker run -d \
  --name phpmyadmin \
  -e PMA_HOST=192.168.1.102 \
  -p 8080:80 \
  --net macvlan \
  phpmyadmin



# Apache configuration for external access
# To add a new configuration

# 1. SSH into your server
# 2. cd /etc/apache2/sites-enabled
# 3. nano watsonserver.org.conf
# 4. Copy / paste configuration, save and exit
# 5. service apache2 restart
