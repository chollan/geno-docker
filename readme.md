# Docker Compose

Helping Geno with his docker setup

## Set Up

Copy the .env.example file to .env and enter the strings for the variables.  Right now it's just the vpn user and password

## Run

To start the services, run the command

    docker-compose -d --env-file .env up

## Update

to update an existing service, run the command

    docker-compose up -d --no-deps <service_name>

## Stop Service

to stop a service type

    docker-compose stop <service>

## Stop all apps

    docker-compose stop